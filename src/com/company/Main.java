
package com.company;
import java.lang.Math;

public class Main {

    public static void main(String[] args) {
        int a = 2;
        int b = 3;
        int c = 1;
        double  x, x1, x2;
        double D;
        D = b*b-4*a*c; //disc
        if(D > 0) {
            x1 = (- b - Math.sqrt(D))/2*a;
            x2 =(- b + Math.sqrt(D))/2*a;
            System.out.println("x1 and x2: " + x1 + " and " + x2);
        } else if(D == 0) {
            x = -b/2*a;
            System.out.println("X: " + x);
        } else {
            System.out.println("No roots");
        }
    }
}
